package ru.tdp;
import java.util.Scanner;

public class Multiplicator {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        double num = in.nextDouble();
        System.out.println("Введите второе число: ");
        double num2 = in.nextDouble();

        System.out.println(multiply(num, num2));
    }

    private static double multiply(double x, double y) {

        if (x == 0 || y == 0) {
            return 0;
        } else if (y > 0) {
            return x + multiply(x, y - 1);
        }
        return -multiply(x, -y);
    }
}