package ru.tdp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Main {
    private static final Path ORIGINALPATH = Paths.get("D:\\Без имени 1.odt");
    private static final Path COPYPATH= Paths.get("D:\\передача\\Без имени 1.odt");
    public static void main(String[] args) {
        try {
            Files.copy(ORIGINALPATH, COPYPATH, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}